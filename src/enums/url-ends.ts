export enum API {
  'POPULAR' = 'movie/popular',
  'OUTCOMING' = 'movie/upcoming',
  'TOP_RATED' = 'movie/top_rated',
  'SEARCH' = 'search/movie',
  'MOVIE' = 'movie'
}