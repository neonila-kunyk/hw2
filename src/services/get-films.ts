import { IMovie } from '../interfaces/movie';
import { IMovies, IExpandedMovie } from '../interfaces/api-res';

import { API } from '../enums/url-ends';

import { API_KEY } from '../../secret';
import { API_URL } from '../../secret';

import { sendRequest } from '../helpers/sender-req';
import { map, transform } from '../helpers/mapper';

export async function get(end: string, page: number): Promise<IMovie[]> {
  let movies = [] as IMovie[];
  try {
    const data = await sendRequest<IMovies>(`${API_URL}/${end}?api_key=${API_KEY}&language=en-US&page=${page}`);
    movies = map(data);
  } catch (e) {
    console.error(e);
  }
  return movies;
}

export async function getPopular(page: number): Promise<IMovie[]> {
  return get(API.POPULAR, page);
}

export async function getUpcoming(page: number): Promise<IMovie[]> {
  return get(API.OUTCOMING, page);
}

export async function getTopRated(page: number): Promise<IMovie[]> {
  return get(API.TOP_RATED, page);
}

export async function getByName(page: number, name: string): Promise<IMovie[]> {
  let movies = [] as IMovie[];
  try {
    const data = await sendRequest<IMovies>(`${API_URL}/${API.SEARCH}?api_key=${API_KEY}&language=en-US&query=${name}&page=${page}`);
    movies = map(data);
  } catch (e) {
    console.error(e);
  }
  return movies;
}

export async function getByIds(ids: string[]): Promise<IMovie[]> {
  const movies = [] as IMovie[];
  try{
    for (const id of ids) {
      const data = await sendRequest<IExpandedMovie>(`${API_URL}/${API.MOVIE}/${id}?api_key=${API_KEY}&language=en-US`);
      movies.push(transform(data));
    }
  } catch (e) {
    console.error(e);
  }
  return movies;
}