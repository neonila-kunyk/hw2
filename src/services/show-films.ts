import { IMovie } from '../interfaces/movie';

import { HEART_COLOR } from '../enums/heart';

import { saveToLS } from '../helpers/saver-favourites';
import { drawHeart } from '../helpers/heart-drawer';

import { getByName, getPopular, getUpcoming, getTopRated, getByIds} from './get-films';

const imgUrl = 'https://image.tmdb.org/t/p/original';

function getRandom(min: number, max: number): number {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

type Card = [ HTMLImageElement, SVGSVGElement, HTMLDivElement, HTMLElement]

function createCard(container:  Element | null, wrapperClasses: string[]): Card {
  const wrapper = document.createElement('div');
  wrapper.classList.add(...wrapperClasses);
  const card = document.createElement('div');
  card.classList.add('card', 'shadow-sm');
  const img = document.createElement('img');
  const heart = drawHeart('unactive');
  const body = document.createElement('div');
  body.classList.add('card-body');
  const description = document.createElement('div');
  description.classList.add('card-text', 'truncate');
  const dateContainer = document.createElement('div');
  dateContainer.classList.add('d-flex', 'justify-content-between', 'align-items-center');
  const date = document.createElement('small');
  date.classList.add('text-muted');
  
  dateContainer.append(date);
  body.append(description, dateContainer);
  card.append(img, heart, body);
  wrapper.append(card);
  container?.append(wrapper);
  
  return [img, heart, description, date];
}

function fillMoviesInfo(movie: IMovie, card: Card, isShowFavorite: boolean) {
  const [img, heart, description, date] = card;
  img.src = `${imgUrl}/${movie.poster_path}`;
  const favorites = JSON.parse(localStorage.getItem('favorites')!) as string[];
  if(favorites?.includes(movie.id.toString())) heart.setAttribute('fill', HEART_COLOR.ACTIVE); 
  heart.getElementsByTagName('path')[0].onclick = () => {
    const isFavorite = saveToLS(movie.id);
    const color = isFavorite ? HEART_COLOR.ACTIVE : HEART_COLOR.UNACTIVE; 
    heart.setAttribute('fill', color);
    if(isShowFavorite) showFavorite();
  };
  description.innerHTML = movie.overview;
  date.innerHTML = movie.release_date;
}

export async function show(movies: IMovie[], firstPage: boolean): Promise<void> {
  const container = document.querySelector('#film-container');
  if(container && firstPage) container.innerHTML = '';
  
  movies.forEach((movie) => {
    const card = createCard(container, ['col-lg-3', 'col-md-4', 'col-12', 'p-2']);
    fillMoviesInfo(movie, card, false);
  });
}

export async function showPopular(page: number, generateRandom: boolean): Promise<void> {
  const movies = await getPopular(page);
  show(movies, page === 1);
  if(generateRandom) showRandomMovie(movies[getRandom(0, movies.length-1)]);
}

export async function showUpcoming(page: number, generateRandom: boolean): Promise<void> {
  const movies = await getUpcoming(page);
  show(movies, page === 1);
  if(generateRandom) showRandomMovie(movies[getRandom(0, movies.length-1)]);
}

export async function showTopRated(page: number, generateRandom: boolean): Promise<void> {
  const movies = await getTopRated(page);
  show(movies, page === 1);
  if(generateRandom) showRandomMovie(movies[getRandom(0, movies.length-1)]);
}

export async function showSearched(page: number, name: string): Promise<void> {
  const movies = await getByName(page, name);
  show(movies, page === 1);
}

export async function showFavoritesMovies(movies: IMovie[]): Promise<void> {
  const container = document.querySelector('#favorite-movies');
  if(container) container.innerHTML = '';
  
  movies.forEach((movie) => {
    const card = createCard(container, ['col-12', 'p-2']);
    fillMoviesInfo(movie, card, true);
  });
}

export async function showFavorite(): Promise<void> {
  const favorites = JSON.parse(localStorage.getItem('favorites')!) as string[];
  if (favorites) {
    const movies = await getByIds(favorites);
    showFavoritesMovies(movies);
  }
}

export async function showRandomMovie(movie: IMovie): Promise<void> {
  const back = document.querySelector('#random-movie') as HTMLElement;
  back.style.backgroundImage = `url(${imgUrl}/${movie.backdrop_path})`;
  back.style.backgroundSize = 'cover';
  back.style.height = '65vh';

  const card = document.querySelector('.col-lg-6.col-md-8.mx-auto') as HTMLElement;
  card.innerHTML = '';
  const title = document.createElement('h1');
  title.classList.add('fw-light', 'text-light');
  title.id = 'random-movie-name';
  title.innerHTML = movie.title;

  const description = document.createElement('p');
  description.classList.add('lead', 'text-white');
  description.id = 'random-movie-description';
  description.innerHTML = movie.overview;

  card.append(title, description);
}