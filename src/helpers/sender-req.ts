export async function sendRequest<T>(url: string): Promise<T> {
  const headers = {
    'Content-Type': 'application/json'
  };
  const response = await fetch(url, {headers});
  if(response.ok) return response.json();
  else throw new Error(response.statusText);
}
