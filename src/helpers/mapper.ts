import { IMovies, IExpandedMovie } from '../interfaces/api-res';
import { IMovie } from '../interfaces/movie';

export function map(data: IMovies): IMovie[] {
  const movies = [] as IMovie[];
  data.results.forEach(movie => {
    const modified = transform(movie);
    movies.push(modified);
  });
  return movies;
}

export function transform(movie: IExpandedMovie): IMovie {
  const modified = {} as IMovie;
  modified.id = movie.id;
  modified.overview = movie.overview;
  modified.poster_path = movie.poster_path;
  modified.backdrop_path = movie.backdrop_path;
  modified.release_date = movie.release_date;
  modified.title = movie.title;
  return modified;
}