export function saveToLS(id: number): boolean {
  let favorites = JSON.parse(localStorage.getItem('favorites')!) as string[];
  if(favorites?.includes(id.toString())) {
    const modified = favorites.filter(item => item !== id.toString());
    localStorage.setItem('favorites', JSON.stringify(modified));
    return false;
  } else {
    if (!favorites) {
      favorites = [] as string[];
    };
    favorites.push(id.toString());
    localStorage.setItem('favorites', JSON.stringify(favorites));
    return true;
  }
  
}