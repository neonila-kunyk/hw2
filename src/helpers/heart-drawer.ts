import { HEART_COLOR } from '../enums/heart';

export function drawHeart(state: string): SVGSVGElement {
  const color = state === 'active' ? HEART_COLOR.ACTIVE : HEART_COLOR.UNACTIVE; 
  const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  svg.setAttribute('width', '50');
  svg.setAttribute('height', '50');
  svg.setAttribute('fill', color);
  svg.setAttribute('stroke', 'red');
  svg.setAttribute('viewBox', '0 -2 18 22');
  svg.classList.add('bi', 'bi-heart-fill', 'position-absolute', 'p-2');

  const path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
  path.setAttribute('fill-rule', 'evenodd');
  path.setAttribute('d', 'M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z');

  svg.appendChild(path);
  return svg;
}