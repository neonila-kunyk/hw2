import { showPopular, showUpcoming, showTopRated, showSearched, showFavorite} from './services/show-films';

function loadHandler(page: number): number {
    page = page + 1;
    const input = document.querySelector('#search') as HTMLInputElement;
    const name = input.value;
    if(name) {
        for (let i=1; i<=page; i++ ) showSearched(i, name);
    } else {
        switch(getState()){
            case 'popular': {
                for (let i=1; i<=page; i++ ) showPopular(i, false);
                break;
            }; 
            case 'upcoming': {
                for (let i=1; i<=page; i++ ) showUpcoming(i, false);
                break;
            };
            case 'top_rated': {
                for (let i=1; i<=page; i++ ) showTopRated(i, false);
                break;
            };
        }
    }
    return page;
}

function unloadHandler(page: number): void {
    switch(getState()){
        case 'popular': {
            showPopular(page, true);
            break;
        }; 
        case 'upcoming': {
            showUpcoming(page, true);
            break;
        };
        case 'top_rated': {
            showUpcoming(page, true);
            break;
        };
    }
}

function getState(): string {
    const buttons = document.querySelectorAll('input[name="btnradio"]') as NodeListOf<HTMLInputElement>;
    let selected = '';
    buttons.forEach((button) => {
        if (button.checked) selected = button.id;
    }); 
    return selected;
}

function reset(): number {
    const input = document.querySelector('#search') as HTMLInputElement;
    input.value = '';
    const page = 1;
    return page;
}

function searchHandler(): string {
    const input = document.querySelector('#search') as HTMLInputElement;
    const name = input.value;
    const radio = document.querySelector(`#${getState()}`) as HTMLInputElement;
    radio.checked = false;
    return name;
}

export async function render(): Promise<void> {
    let page = 1;
    unloadHandler(page);

    document.querySelector('#popular')?.addEventListener('click', () => {
        page = reset();
        showPopular(page, false);
    });
    document.querySelector('#upcoming')?.addEventListener('click', () => {
        page = reset();
        showUpcoming(page, false);
    });
    document.querySelector('#top_rated')?.addEventListener('click', () => {
        page = reset();
        showTopRated(page, false);
    });
    document.querySelector('#submit')?.addEventListener('click', () => {
        const page = 1;
        const  name = searchHandler();
        showSearched(page, name);
    });
    document.querySelector('#load-more')?.addEventListener('click', () => page = loadHandler(page));
    document.querySelector('.navbar-toggler')?.addEventListener('click', () => showFavorite());
    document.querySelector('.btn-close')?.addEventListener('click', () => loadHandler(page-1));
    document.querySelector('#offcanvasRight')?.addEventListener('blur', () => loadHandler(page-1));
}