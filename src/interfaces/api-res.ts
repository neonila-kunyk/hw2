import { IMovie } from '../interfaces/movie';

export interface IExpandedMovie extends IMovie{
  [key: string]: any
}

export interface IMovies {
  page: number
  results: IExpandedMovie[]
  total_pages: number,
  total_results: number
}
